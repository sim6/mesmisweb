<div id="main">
  <h1>Creditos</h1>
  <div class="separator"></div>
  <p>
     Responsable de contenido y diseño general<br />
     <strong>Yankuic Galván-Miyoshi</strong>
  </p>
  <p>
     Diseño gráfico<br />
     <strong>Raul Montero</strong>
  </p>
  <p>
     Fotografía<br />
     <strong>Yankuic Galván-Miyoshi</strong><br />
     <strong>Andrés Camou Guerrero </strong>
  </p>
  <p>
     Programación<br />
     <strong>Max Pimm</strong><br />
     <strong>Simó Albert</strong>
  </p>
  <p>
     Traducciones a Inglés<br />
     <strong>Gabriela Tobias</strong><br />
     <strong>Diane Miyoshi</strong>
  </p>
  <div class="separator"></div>
  <p>
     El desarrollo y actualización de esta página se hizo con apoyo financiero del proyecto CONACYT 51293 Estudios de Sustentablidad de Sistemas Complejos Socioambientales.
  </p>
</div>




