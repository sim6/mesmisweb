<div id="leftSideBar">
  <ul>
    <li style="border: none">
      <div><a class="arrow" href="#1">>></a><a href="#1">MESMIS Framework</a></div>
      <ul>
        <li style="border-top: 1px dashed #666"><div><a class="arrow" href="#1.1">>></a><a href="#1.1">BACKGROUND</a></div></li>
        <li><div><a class="arrow" href="#1.2">>></a><a href="#1.2">WHAT IS MESMIS?</a></div></li>
        <li><div><a class="arrow" href="#1.3">>></a><a href="#1.3">WHO IS IT FOR?</a></div></li>
        <li><div><a class="arrow" href="#1.3">>></a><a href="#1.4">OPERATIONAL STRUCTURE</a></div></li>
      </ul>
    </li>
    <li><div><a class="arrow" href="#2">>></a><a href="#2">OTHER MATERIALS</a></div></li>
  </ul>
</div>
<div>
  <div id="staticContent">
    <a name="1"></a>
    <h1>THE <strong>MESMIS</strong> FRAMEWORK</h1>
    <div class="separator"></div>
    <div class="section">
      <a name="1.1"></a>
      <h2>BACKGROUND</h2>
      <p>In spite of the upsurge in the discussion about sustainable development, there are still few systematic, consistent initiatives to operationalize the general principles of sustainability in concrete case studies. Conventional procedures (for example, cost-benefit analysis) are insufficient or simply unsuitable for incorporating the new challenges of sustainability analysis, such as the existence of non-quantifiable variables and the integration of biophysical parameters together with social and economic processes.</p>
      <p>Attempts to mitigate the problem have been mostly “additive”, that is, they have added conventional techniques. However, a qualitatively different conceptual and practical effort is needed. Particularly, there are no frameworks developed for evaluating natural resource management systems in the context of small farmers in Third World countries. There is a relatively large body of work on biophysical-type indicators, especially for very concrete subsystems in controlled conditions (sustainability indicators for soil or for a given crop). There are also relatively well-developed economic indicators, mainly for commercial agriculture. However, there has not been sufficient work in the incorporation of social and institutional criteria in complex management systems (for example, silvopasture and agroforestry systems) or in systems with long-term rotation cycles such as forests.</p>
      <p>Furthermore, proposals for evaluating sustainability either come from lax frameworks aiming for “rapid” assessments, or are so detailed that they can be carried out only under experimental conditions. More effort is needed to establish frameworks that are truly operational in field conditions and that are simultaneously based on a rigorous evaluation of sustainability.</p>
      <p>Finally, sustainability analyses generally tend merely to qualify the systems under analysis (i.e., they end in statements such as “the system is or is not sustainable”). Procedures have not yet been developed to orient sustainability assessments to the improvement the socio-environmental profile of the diverse management systems or technologies.</p>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
    <div class="section">
      <a name="1.2"></a>
      <h2>WHAT IS THE MESMIS? </h2>
      <p>The Framework for the Evaluation of Natural Resource Management Systems Incorporating Sustainability Indicators (in Spanish <strong>M</strong>arco para la <strong>E</strong>valuaciÃ³n de <strong>S</strong>istemas de <strong>M</strong>anejo de recursos naturales incorporando <strong>I</strong>ndicadores de <strong>S</strong>ustentabilidad, MESMIS) is a methodological tool that: </p>
      <ul>
        <li>Helps in evaluating the sustainability of natural resource management systems (NRMS), with an emphasis on small farmers and local contexts (from the farm level to the village level)</li>
        <li>Aims at improving the NRMS under evaluation. MESMIS is a process of <em>analysis and feedback</em>, seeking to avoid a simple ranking of management systems in terms of a <em>sustainability index or scale</em>.</li>
        <li>Seeks an integral understanding of the opportunities and constraints for the sustainability of NRMS that are forged by the intersection of environmental processes and socio-economic conditions </li>
        <li><em>Compares</em> management systems, either by contrast to one or more reference, or by observing changes in the properties of a particular system over a period of time</li>
        <li>Has a flexible structure so that it can be adapted to different levels of information and technical skills that are available locally. Moreover, it proposes a process of participatory evaluation that emphasizes group dynamics and continuous feedback from the evaluating team.</li>
        <li>Constitutes a developing tool, which benefits from the experience of its application in different socio-ecological contexts.</li>
      </ul>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
    <div class="section">
      <a name="1.3"></a>
      <h2>WHO IS IT FOR?</h2>
      <p>MESMIS is appropriate for research institutions, nongovernmental organizations and producer associations involved in the design, development and the dissemination of natural resources management systems.</p>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
    <div class="section">
      <a name="1.4"></a>
      <h2>OPERATIONAL STRUCTURE </h2>
      <p>The main objective of MESMIS is to provide a methodological framework for evaluating the sustainability of different natural resources management systems at a local scale (plot, farm or productive unit, community), based on the following premises: </p>
      <ul>
        <li>Sustainability is defined by five general attributes of natural resource management systems: (a) productivity, (b) stability, reliability and resilience (c) adaptability, (d) equity and (e) self-reliance.</li>
        <li>Evaluating sustainability is valid only in the context of: (a)specific management systems in a given geographical location and in a given social and political context; (b)a previously determined spatial scale (plot, production unit, community or watershed); and (c)a previously determined time scale.</li>
        <li>Evaluating sustainability is an interdisciplinary and participatory process. The evaluation team must include both external evaluators and direct stakeholder (farmers, technicians, community representatives and other actors)</li>
        <li>Sustainability cannot be evaluated per se; rather, it should be evaluated against a reference case, i.e., it is a relative measure. There are two ways to achieve this: (a) by comparing the evolution of a single system over time (longitudinal) or (b) by simultaneously comparing one or more alternative or innovative management systems with a reference system (cross-sectional).</li>
        <li>Sustainability evaluation is a cyclical process aimed at strengthening both the management systems and the evaluation methodology itself.</li>
      </ul>
      <p> Operationally, to “ground” the general sustainability attributes, a series of critical points, or systems strengths and weaknesses, for the sustainability of the management system are defined. These critical points relate to three evaluation areas: environmental, social and economic. In each area of evaluation, diagnostic criteria and indicators are defined. This mechanism assures a clear relationship between the indicators and the sustainability characteristics of the agro-ecosystem. (See diagram 1).</p>
      <p> Finally, the information obtained through the different indicators is integrated using multi-criteria analysis techniques, with the aim of giving a value judgment of the management systems and providing suggestions for the improvement of its socio-environmental profile.</p>
      <br /><br />
      <?php echo image_tag('static/estructura_operativa_en.png', 'alt="Figura 1. Operative structure: the relation between attributes and indicators"') ?>
      <br /><br />
      <p align="center">Diagram 1. Operational structure: the relationship between attributes and indicators</p>
      <p> To apply the methodology, we propose an evaluation cycle, comprising the following elements or steps (See diagram 2):</p>
      <ul>
        <li>Define the object (target) of the evaluation. In this step the management systems to be evaluated are defined, its characteristics and the socio-environmental context of evaluation.</li>
        <li>Determine the strengths and weaknesses that can have an effect on the sustainability of the management systems to be evaluated</li>
        <li>Select indicators. In this step the diagnostic criteria are determined and the strategic indicators are derived</li>
        <li>Measure and monitor indicators. This step includes designing the instruments of analysis and obtaining the desired information </li>
        <li>Integrate results. Here, we compare sustainability of the management systems under analysis, and the main obstacles to sustainability are indicated as are the aspects that most contribute to sustainability of the system</li>
        <li>Conclusions and recommendations. Finally, in this step a synthesis of the analysis is made and suggestions are given to fortify sustainability of the management systems, as well as to improve the evaluation process itself</li>
      </ul>
      <p> Once these six steps are covered, a new conceptualization of the systems under analysis is achieved and the aspects that need improvement to make them more sustainable higlighted. With this, we begin a new evaluation cycle.</p>
      <br /><br />
      <?php echo image_tag('static/ciclo_mesmis_en.png', 'alt="Figura. 2. El ciclo de evaluaciÃ³n"') ?>
      <br /><br />
      <p align="center">Diagram 2. The evaluation cycle</p>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
    <div class="section">
      <a name="2"></a>
      <h2>OTHER REFERENCE MATERIALS.</h2>
      <p><a href="http://mesmis.gira.org.mx/es/products#pg122" target="blank">Sustainability collection</a></p>
      <p><a href="http://mesmis.gira.org.mx/es/products#pg121" target="blank">MESMIS interactive</a>
      <p><?php echo link_to('Support material', 'product/index') ?></p>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
  </div>
</div>