<div id="main">
    <h1><?php echo __('Página no encontrada') ?></h1>
    <div class="separator"></div>
    <p><?php echo __('No se ha encontrado la página solicitada. Por favor comprueba la URL y vuelva a intentar.') ?></p>
    <br />
    <a class="btn" href="" onclick="history.go(-1);return false"><?php echo __('Volver') ?></a>
</div>
